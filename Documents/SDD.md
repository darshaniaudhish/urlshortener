## Validation scenarios of url

1. Error message if the URL format is incorrect. 
2. For a same Url, same mini url should be generated.
3. Check if the miniurl is already existing.. 
4. Do not store the same url
5. Send a response only when update is completed.
6. Add authentication and Authorization 



## Entity model

{
    objectId: String(60),
    url: String(120),
    miniUrl: String(60),


}