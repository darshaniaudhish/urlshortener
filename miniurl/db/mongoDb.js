const mongoClient = require('mongodb').MongoClient;
const dbOper = require('./operations');
const assert = require('assert');
const constants = require('./../constants/miniUrlConstants');
// const async = require('async');
// const await = require('await');

const dbName = 'miniUrl';
const collection = 'url';

function insertDocument(document) {
    client = new mongoClient(url, {
        useUnifiedTopology: true
    });

    client.connect()
        .then((client) => {
            var db = client.db(dbName);
            console.log('Connection established successfully');
            dbOper.insertDocument(db, document, collection)
                .then((result) => {
                    console.log(result.ops, ' inserted');
                    client.close();
                    return result;    
                })
                .catch((error) => {
                    console.log(error);
                    client.close();
                    return error;
                });
        })
        .catch((err) => {
            console.log(err.stack);
            console.log('Error occurred while connecting to database');            
            return err;
        });
}

async function findDocument(document){
    client = new mongoClient(constants.DB_HOST, {
        useUnifiedTopology: true
    });
    
    const client1 = await client.connect()
    .catch((error) => {
        console.log(error.stack);
        console.log('Error connecting database');
        return(error);
    });
    if(!client1){
        return;
    }

    try{
        console.log('Database connection successfully established');
        var db = client1.db(dbName);
        let coll = db.collection(collection);
        var miniUrl = document.miniUrl;
        document.miniUrl = constants.HOST + '/'+ document.miniUrl;
        let result = await coll.findOne(document);
        console.log({result});
        if(result!=null){
            var url = result.url;
            return url;

        }else{
            console.log(`Url ${miniUrl} not found`);
            return null;
        }


    }catch(err){
        console.log(err.stack);
        console.log('Error finding document in database');
        return null;
    }        

}


function connect() {

    client = new mongoClient(url, {
        useUnifiedTopology: true
    });

    client.connect()
        .then((client) => {
            dbConnection = client.db(db);
            console.log('Connection established successfully')
        })
        .catch((err) => {
            console.log(err.stack);
            console.log('Error occurred while connecting to database');
        });
    return client;
}

function close() {
    client.close();
    console.log('Database connection');
}

module.exports = {
    connect,
    close,
    insertDocument,
    findDocument
};