const mongoDbConnection = require('./mongoDb');
const assert = require('assert');

// var client = mongoDbConnection.connect();
// if(client.isConnected()){
//     mongoDbConnection.close();
// }

exports.insertDocument = (db, document, collection) =>{
    const coll = db.collection(collection);
    return coll.insertOne(document);
};

exports.findDocument = (db,document,collection) => {
    const coll = db.collection(collection);
    return coll.findOne(document);
};

