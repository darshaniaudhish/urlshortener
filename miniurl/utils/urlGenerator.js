const constants =  require('./../constants/miniUrlConstants');

exports.randomString = ()=>{
   var result = [];
   while(constants.URL_LENGTH--){
      result += constants.CHAR_SET.charAt(Math.floor(Math.random() * constants.CHAR_SET.length));
   }
   return result;
}

exports.validateUrl = (url) =>{
   var regExp = new RegExp(constants.URL_REGEX); 
   return regExp.test(url);
};


