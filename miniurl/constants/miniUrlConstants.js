const URL_LENGTH = 15;
const CHAR_SET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
const HOST = "https://miniurl.com";
const DB_HOST = "mongodb://localhost:27017";
const URL_REGEX = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$";

module.exports = {URL_LENGTH,CHAR_SET,HOST, DB_HOST,URL_REGEX};

