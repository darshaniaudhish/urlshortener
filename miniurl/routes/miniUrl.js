var express = require('express');
const bodyParser = require('body-parser');
require('cluster')
const { request, response } = require('../app');
const constants = require('./../constants/miniUrlConstants');
const util = require('./../utils/urlGenerator');
const mongoDbConnection = require('./../db/mongoDb');
var async = require('async');
var await = require('await');


var miniUrlRouter = express.Router();

miniUrlRouter.use(bodyParser.json());

miniUrlRouter.route('/')
.all((request, response, next) => {
    response.status(200);
    response.setHeader("Content-Type","text/plain");
    next();
})

.post((request,response,next) => {

    console.log(request.body.url);
    var url = request.body.url;
    if(util.validateUrl(url)){
        var endpoint = util.randomString();
        var host = constants.HOST;
        var miniUrl = host + '/'+ endpoint;
        var result = documentInsert(url,miniUrl);      

        response.end("Updated the url "+ result);
    }else{
        response.status(400).send("Invalid Url provided");
    }
        
});

miniUrlRouter.get('/:miniUrl',async (request,response,next) =>{
    var miniUrl = request.params.miniUrl;
    console.log('miniUrl: ' + miniUrl);
    var url = await documentFind(miniUrl);
    console.log(url);
    if(url!=null){
        response.redirect(301,url);
    }else{
        response.status(400).send('MiniUrl does not exist');
    }    

});

async function documentInsert(url,miniUrl){
    var result = await mongoDbConnection.insertDocument({"url": url,"miniUrl":miniUrl});
    return result;
}

function documentFind(miniUrl){
    var url = mongoDbConnection.findDocument({"miniUrl" : miniUrl});
    return url;
}

module.exports = miniUrlRouter;